package com.moien.bamantest;

import android.app.Application;

import com.moien.bamantest.di.component.AppComponent;
import com.moien.bamantest.di.component.DaggerAppComponent;
import com.moien.bamantest.di.module.AppModule;
import com.moien.bamantest.di.module.UtilsModule;
import com.moien.bamantest.network.Urls;

import javax.inject.Singleton;

import dagger.Component;


public class TheApplication extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = createAppComponent();


    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private AppComponent createAppComponent() {
        return DaggerAppComponent
                .builder()
                .application(this)
                .appModule(new AppModule(this))
                .utilsModule(new UtilsModule(Urls.BASE_URL))
                .build();
    }
}
