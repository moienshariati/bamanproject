package com.moien.bamantest.view.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.moien.bamantest.R;
import com.moien.bamantest.databinding.LCardHomeRecBinding;
import com.moien.bamantest.model.ListHome;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    List<ListHome> listHomes;
    private Context context;
    private LCardHomeRecBinding binding;
    private NavController navController;

    public HomeAdapter(List<ListHome> listHomes, Context context) {
        this.listHomes = listHomes;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        return new ViewHolder(LCardHomeRecBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String dateMiladi;
        ListHome listHome = listHomes.get(position);
        holder.binding.tvName.setText(listHome.getName());
        holder.binding.tvDescrip.setText(listHome.getDescription());
        Picasso.get()
                .load(listHome.getImageUrl())
                .into(holder.binding.ivCardHome);
        listHome.getCreateDate();

        dateMiladi = getDate(listHome.getCreateDate());


    }

    @Override
    public int getItemCount() {
        return listHomes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        private LCardHomeRecBinding binding;

        public ViewHolder(LCardHomeRecBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


            binding.cvHomeMain.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putString("id", String.valueOf(listHomes.get(getAdapterPosition()).getId()));
                Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_detailHomeFragment,bundle);
            });

        }





    }


//    private String getDate(long time) {
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(time*1000);
//        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
//        return date;
//    }

    private String getDate(long time) {
        Date date = new Date(time * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy "); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));

        return sdf.format(date);
    }


}
