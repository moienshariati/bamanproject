package com.moien.bamantest.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.airbnb.lottie.LottieAnimationView;
import com.moien.bamantest.TheApplication;
import com.moien.bamantest.databinding.LDetailHomeFragmentBinding;
import com.moien.bamantest.model.ApiResponseModel;
import com.moien.bamantest.model.DetailsHomeModel;
import com.moien.bamantest.viewmodel.AuthViewModel;
import com.moien.bamantest.viewmodel.ViewModelFactory;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class DetailHomeFragment extends Fragment {
    private TextView tvName, tvDescr, tvDate;
    private ImageView ivDetail, ivShareLink;
    private String idDetail, url;
    private Date strDate;
    private LottieAnimationView progressBar;

    private LDetailHomeFragmentBinding binding;
    private NavController navController;
    @Inject
    ViewModelFactory viewModelFactory;
    AuthViewModel authViewModel;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = LDetailHomeFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((TheApplication) getActivity().getApplicationContext())
                .getAppComponent().doInjectionDetailsFragment(this);

        getBundle();
        init();

        getDetailApi(idDetail);

        setOnclickListeners();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
    }

    private void init() {
        tvName = binding.tvNameDetail;
        tvDescr = binding.tvDescDetail;
        tvDate = binding.tvDate;
        ivDetail = binding.ivDetails;
        ivShareLink = binding.ivShareLink;

        progressBar=binding.progressDetails;
    }

    private void getDetailApi(String id) {
        authViewModel = ViewModelProviders.of(this, viewModelFactory).get(AuthViewModel.class);

        authViewModel.callGetDetails().observe(getViewLifecycleOwner(), new Observer<ApiResponseModel<DetailsHomeModel>>() {
            @Override
            public void onChanged(ApiResponseModel<DetailsHomeModel> detailsHomeModelApiResponseModel) {
                if (detailsHomeModelApiResponseModel.getMode() == ApiResponseModel.Mode.SUCCESS) {
                    progressBar.setVisibility(View.GONE);
                    tvName.setText(detailsHomeModelApiResponseModel.getData().getData().getName());
                    tvDescr.setText(detailsHomeModelApiResponseModel.getData().getData().getDescription());
                    Picasso.get()
                            .load(detailsHomeModelApiResponseModel.getData().getData().getImageUrl())
                            .into(ivDetail);
                    String date = formatDate(detailsHomeModelApiResponseModel.getData().getData().getCreateDate());
                    tvDate.setText(persianDate(date).toString());
                    url = "https://" + detailsHomeModelApiResponseModel.getData().getData().getShareUrl();

                } else if (detailsHomeModelApiResponseModel.getMode() == ApiResponseModel.Mode.LOADING) {
                    progressBar.setVisibility(View.VISIBLE);

                }
            }
        });

        authViewModel.hitGetHomeDetails(id);

    }

    private void setOnclickListeners() {
        ivShareLink.setOnClickListener(v -> intentBrowser());
    }

    private void getBundle() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idDetail = bundle.getString("id");
        }
    }

    //convert stamp time to  string
    private String formatDate(long milliseconds) /* This is your topStory.getTime()*1000 */ {
        DateFormat sdf = new SimpleDateFormat("yyyy-mm-dd' 'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        TimeZone tz = TimeZone.getDefault();
        sdf.setTimeZone(tz);
        return sdf.format(calendar.getTime());
    }

    //convert string to persian date
    private PersianDate persianDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        try {
            strDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        PersianDate pdate = new PersianDate(strDate);
        PersianDateFormat pdformater = new PersianDateFormat();
        pdformater.format(pdate);
        return pdate;
    }

    private void intentBrowser() {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

}
