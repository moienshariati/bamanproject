package com.moien.bamantest.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.moien.bamantest.databinding.LNootbookFragmentBinding;

public class NoteBookFragment extends Fragment {

    private LNootbookFragmentBinding binding;
     NavController navController;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = LNootbookFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();



        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        navController = Navigation.findNavController(view);
    }


}
