package com.moien.bamantest.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.moien.bamantest.TheApplication;
import com.moien.bamantest.databinding.LHomeFragmentBinding;
import com.moien.bamantest.model.ApiResponseModel;
import com.moien.bamantest.model.BodyHomeList;
import com.moien.bamantest.model.HomeModel;
import com.moien.bamantest.model.ListHome;
import com.moien.bamantest.view.adapter.HomeAdapter;
import com.moien.bamantest.viewmodel.AuthViewModel;
import com.moien.bamantest.viewmodel.BussinesViewModel;
import com.moien.bamantest.viewmodel.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;

    AuthViewModel authViewModel;
    BussinesViewModel bussinesViewModel;

    private HomeAdapter homeAdapter;
    private RecyclerView recyclerView;

    private LottieAnimationView progressbar;

    private List<ListHome> listHomes = new ArrayList<>();

    private BodyHomeList bodyHomeList = new BodyHomeList();

    private TextView tvTitle;

    private Button btnYekTop, btnDoTop, btnSeTop, btnCharTop, btnPanjTop, btnShishTop;
    private Button btnYekBot, btnDoBot, btnSeBot, btnCharBot, btnPanjBot, btnShishBot;
    private LHomeFragmentBinding binding;

    NavController navController;
     Context context;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = LHomeFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ((TheApplication) getActivity().getApplicationContext())
                .getAppComponent().doInjection(this);


        init();
        initAdapter();
        postHomeApi("1");
        onClickListeners();

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
    }

    private void init() {

        recyclerView = binding.rcHome;
        progressbar=binding.progressBar;
        tvTitle=binding.tvTitleHome;

        btnYekTop = binding.btnYekTop;
        btnDoTop = binding.btnDoTop;
        btnSeTop = binding.btnSeTop;
        btnCharTop = binding.btnCharTop;
        btnPanjTop = binding.btnPanjTop;
        btnShishTop = binding.btnShishTop;

        btnYekBot = binding.btnYekBot;
        btnDoBot = binding.btnDoBot;
        btnSeBot = binding.btnSeBot;
        btnCharBot = binding.btnCharBot;
        btnPanjBot = binding.btnPangBot;
        btnShishBot = binding.btnShishBot;


    }

    private void initAdapter() {
        homeAdapter = new HomeAdapter(listHomes, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);

        recyclerView.setAdapter(homeAdapter);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void postHomeApi(String id) {
        bussinesViewModel=ViewModelProviders.of(this,viewModelFactory).get(BussinesViewModel.class);
        authViewModel = ViewModelProviders.of(this, viewModelFactory).get(AuthViewModel.class);

        authViewModel.callPostHomeModelLive().observe(getViewLifecycleOwner(), new Observer<ApiResponseModel<HomeModel>>() {
            @Override
            public void onChanged(ApiResponseModel<HomeModel> homeModelApiResponseModel) {
                if (homeModelApiResponseModel.getMode() == ApiResponseModel.Mode.SUCCESS) {
                    progressbar.setVisibility(View.GONE);

                    tvTitle.setText(homeModelApiResponseModel.getData().getData().getTitle());
                    listHomes.clear();

                    for (int i = 0; i < homeModelApiResponseModel.getData().getData().getList().size(); i++) {
                        listHomes.add(homeModelApiResponseModel.getData().getData().getList().get(i));
                    }

                    homeAdapter.notifyDataSetChanged();

                } else if (homeModelApiResponseModel.getMode() == ApiResponseModel.Mode.LOADING) {

                    progressbar.setVisibility(View.VISIBLE);

                } else if (homeModelApiResponseModel.getMode() == ApiResponseModel.Mode.ERROR) {
                    progressbar.setVisibility(View.GONE);


                    bussinesViewModel.makeToast(getContext(),"لطفا فیلتر شکن وصل کنید");
                }
            }

        });

        bodyHomeList.setPage(id);
        authViewModel.hitCallHomeList(bodyHomeList);
    }

    private void onClickListeners() {

        btnYekTop.setOnClickListener(v -> postHomeApi("1"));
        btnDoTop.setOnClickListener(v -> postHomeApi("2"));
        btnSeTop.setOnClickListener(v ->postHomeApi("3"));
        btnCharTop.setOnClickListener(v ->postHomeApi("4"));
        btnPanjTop.setOnClickListener(v ->postHomeApi("5"));
        btnShishTop.setOnClickListener(v ->postHomeApi("6"));

        btnYekBot.setOnClickListener(v -> postHomeApi("1"));
        btnDoBot.setOnClickListener(v -> postHomeApi("2"));
        btnSeBot.setOnClickListener(v -> postHomeApi("3"));
        btnCharBot.setOnClickListener(v -> postHomeApi("4"));
        btnPanjBot.setOnClickListener(v -> postHomeApi("5"));
        btnShishBot.setOnClickListener(v -> postHomeApi("6"));

    }
}
