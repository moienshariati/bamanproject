package com.moien.bamantest.view.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.moien.bamantest.R;
import com.moien.bamantest.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {



    private BottomNavigationView bottomNavigation;
    private ActivityMainBinding binding;
     NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();

        initBottomNav();



    }

    private void init() {
        bottomNavigation =binding.meowBottomNavigation;
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);


        bottomNavigation.setOnNavigationItemSelectedListener(item -> false);


                NavigationUI.setupWithNavController(bottomNavigation,navController);
    }

    private void initBottomNav() {


    }
}