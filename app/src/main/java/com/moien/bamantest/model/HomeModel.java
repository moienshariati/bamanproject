package com.moien.bamantest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeModel extends ApiDataModel {
    @SerializedName("data")
    @Expose
    private DataHome data;
    @SerializedName("code")
    @Expose
    private Integer code;

    public DataHome getData() {
        return data;
    }

    public void setData(DataHome data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
