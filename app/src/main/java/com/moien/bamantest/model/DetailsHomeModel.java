package com.moien.bamantest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsHomeModel extends ApiDataModel{
    @SerializedName("data")
    @Expose
    private DataDetailsHome data;
    @SerializedName("code")
    @Expose
    private Integer code;

    public DataDetailsHome getData() {
        return data;
    }

    public void setData(DataDetailsHome data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
