package com.moien.bamantest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyHomeList {
    @SerializedName("page")
    @Expose
    private String page;

    public BodyHomeList() {
    }

    public BodyHomeList(String page) {
        this.page = page;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

}
