package com.moien.bamantest.model;

import androidx.annotation.NonNull;

public class ApiResponseModel<T extends ApiDataModel> {
    boolean status;
    String error;
    Mode mode;
    T data;
    Throwable throwable;

    public ApiResponseModel(boolean status, String error, Mode mode, T data) {
        this.status = status;
        this.error = error;
        this.mode = mode;
        this.data = data;
    }

    public ApiResponseModel(boolean status, String error, Mode mode, T data, Throwable throwable) {
        this.status = status;
        this.error = error;
        this.mode = mode;
        this.data = data;
        this.throwable = throwable;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public static ApiResponseModel loading() {
        return new ApiResponseModel(true, null, Mode.LOADING, null);

    }

    public static ApiResponseModel success(@NonNull ApiDataModel data) {
        return new ApiResponseModel(true, null, Mode.SUCCESS, data);
    }

    public static ApiResponseModel error(@NonNull Throwable error) {
        return new ApiResponseModel(false, error.getMessage(), Mode.ERROR, null,error);
    }


    public enum Mode {
        LOADING,
        SUCCESS,
        ERROR
    }

}
