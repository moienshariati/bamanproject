package com.moien.bamantest.repositories;

import com.moien.bamantest.model.BodyHomeList;
import com.moien.bamantest.model.DataHome;
import com.moien.bamantest.model.DetailsHomeModel;
import com.moien.bamantest.model.HomeModel;
import com.moien.bamantest.model.ListHome;
import com.moien.bamantest.network.ApiCallInterface;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Query;

public class ApiRepository {
    private ApiCallInterface apiCallInterface;

    @Inject
    public ApiRepository(ApiCallInterface apiCallInterface) {
        this.apiCallInterface = apiCallInterface;
    }

    public Observable<HomeModel> executePostList(@Body BodyHomeList number) {
        return apiCallInterface.requestList(number);
    }

    public Observable<DetailsHomeModel> executeGetDetail(@Query("id")String id){
        return apiCallInterface.requestDetail(id);
    }

}
