package com.moien.bamantest.network;

import com.moien.bamantest.model.BodyHomeList;
import com.moien.bamantest.model.DetailsHomeModel;
import com.moien.bamantest.model.HomeModel;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiCallInterface {

    @POST(Urls.AUTH_POST_LIST)
    Observable<HomeModel> requestList(@Body BodyHomeList number);

    @GET(Urls.AUTH_GET_BYID)
    Observable<DetailsHomeModel> requestDetail(@Query("id") String id);
}
