package com.moien.bamantest.di.component;

import android.app.Application;

import com.moien.bamantest.di.module.AppModule;
import com.moien.bamantest.di.module.UtilsModule;
import com.moien.bamantest.view.fragment.DetailHomeFragment;
import com.moien.bamantest.view.fragment.HomeFragment;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = {AppModule.class, UtilsModule.class})
@Singleton
public interface AppComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        AppComponent.Builder application(Application application);
        AppComponent.Builder appModule(AppModule appModule);
        AppComponent.Builder utilsModule(UtilsModule utilsModule);
        AppComponent build();

    }
    void doInjection(HomeFragment fragment);
    void doInjectionDetailsFragment(DetailHomeFragment fragment);
}
