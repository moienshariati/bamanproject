package com.moien.bamantest.di.module;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.moien.bamantest.network.ApiCallInterface;
import com.moien.bamantest.repositories.ApiRepository;
import com.moien.bamantest.viewmodel.ViewModelFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class UtilsModule {

    private String baseUrl;
    public UtilsModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    public Gson provideGson() {

        GsonBuilder builder =
                new GsonBuilder();
        return builder.setLenient().create();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson , OkHttpClient okHttpClient) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    @Provides
    @Singleton
    public ApiCallInterface getApiCallInterface(Retrofit retrofit) {
        return retrofit.create(ApiCallInterface.class);
    }

    @Provides
    @Singleton
    public OkHttpClient getRequestHeader() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder().build();
            return chain.proceed(request);
        })
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();
        return client;
    }

    @Provides
    @Singleton
    public ApiRepository getApiRepository(ApiCallInterface apiCallInterface) {
        return new ApiRepository(apiCallInterface);
    }

    @Provides
    @Singleton
    public ViewModelProvider.Factory getViewModelFactory(ApiRepository apiRepository) {
        return new ViewModelFactory(apiRepository);
    }
}
