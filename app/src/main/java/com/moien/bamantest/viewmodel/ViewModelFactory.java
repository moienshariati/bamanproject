package com.moien.bamantest.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.moien.bamantest.repositories.ApiRepository;

import javax.inject.Inject;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private ApiRepository apiRepository;

    @Inject
    public ViewModelFactory(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AuthViewModel.class))
            return (T) new AuthViewModel(apiRepository);
        else if (modelClass.isAssignableFrom(BussinesViewModel.class))
            return (T)new BussinesViewModel();
        throw new IllegalArgumentException("Unknown class name");

    }
}
