package com.moien.bamantest.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.moien.bamantest.model.ApiResponseModel;
import com.moien.bamantest.model.BodyHomeList;
import com.moien.bamantest.model.DetailsHomeModel;
import com.moien.bamantest.model.HomeModel;
import com.moien.bamantest.repositories.ApiRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;

public class AuthViewModel extends ViewModel {
    private Context context;
    private ApiRepository apiRepository;
    private final CompositeDisposable disposables = new CompositeDisposable();

    private final MutableLiveData<ApiResponseModel<HomeModel>> callPostListLive = new MutableLiveData<>();
    private final MutableLiveData<ApiResponseModel<DetailsHomeModel>> callGetDetail = new MutableLiveData<>();

    @Inject
    public AuthViewModel(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    public MutableLiveData<ApiResponseModel<HomeModel>> callPostHomeModelLive() {
        return callPostListLive;
    }

    public MutableLiveData<ApiResponseModel<DetailsHomeModel>> callGetDetails() {
        return callGetDetail;
    }


    public void hitCallHomeList(@Body BodyHomeList number) {
        disposables.add(apiRepository.executePostList(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> callPostListLive.setValue(ApiResponseModel.loading()))
                .subscribe(new Consumer<HomeModel>() {
                               @Override
                               public void accept(HomeModel homeModel) throws Exception {
                                   for (int i = 0; i < homeModel.getData().getList().size(); i++) {


                                   }
                                   callPostListLive.setValue(ApiResponseModel.success(homeModel));

                               }

                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                  callPostListLive.setValue(ApiResponseModel.error(throwable));
                               }
                           }
                ));
    }

    public void hitGetHomeDetails(String id) {
        disposables.add(apiRepository.executeGetDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> callGetDetail.setValue(ApiResponseModel.loading()))
                .subscribe(new Consumer<DetailsHomeModel>() {
                    @Override
                    public void accept(DetailsHomeModel detailsHomeModel) throws Exception {
                        callGetDetail.setValue(ApiResponseModel.success(detailsHomeModel));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d(":::", "accept: ");
                    }
                }));
    }

    protected void onCleared() {
        disposables.clear();
    }

}
